import math

class SequentialPatterns:
    def __init__(self):
        self.absolute_support = 99
        self.lines = []  # Array to store the lines from the file
        self.n_1_patterns = [] # Array to temporarily store all candidate frequent patterns at n-1 level
        self.n_patterns = {} # Dictionary to store all frequent patterns with count at n level

    def insert_line(self, line):
        self.lines.append(line)

    def index_in_list(self, a_list, index):
        return index < len(a_list)

    def generate_candidate_patterns_and_count(self):
        # Iterate through n-1 level patterns and generate n level patterns
        for i in range(len(self.n_1_patterns)):
            for j in range(len(self.n_1_patterns)):
                pattern = self.n_1_patterns[i] + " " + self.n_1_patterns[j]
                self.n_patterns.append(pattern)
                # Get count of pattern in database
                for line in self.lines:
                    if pattern in line:
                        if pattern not in self.patterns:
                            self.patterns[pattern] = 1
                        else:
                            self.patterns[pattern] = int(self.patterns[pattern]) + 1
                if pattern in self.patterns and self.patterns[pattern] > self.absolute_support:
                    print(pattern, self.patterns[pattern])

    def generate_frequent_1_patterns(self):
        # Iterate through each file and count unique words and store in dictionary for each line
        for line in self.lines:
            words = line.rstrip().split(' ')
            unique_words = []
            [unique_words.append(x) for x in words if x not in unique_words]
            # Insert all unique words in dictionary and increment their count by 1
            for word in unique_words:
                if word not in self.n_patterns:
                    self.n_patterns[word] = 1
                else:
                    self.n_patterns[word] = int(self.n_patterns[word]) + 1
        for key, value in self.n_patterns.items():
            if value > self.absolute_support:
                self.output(key,value)
                self.n_1_patterns.append(key)
        self.n_patterns = {}
        print(len(self.n_1_patterns))
        print(self.n_1_patterns)


    def pattern_match(self, line, pattern):
        # Get array of all words in line, Get array of all words in pattern.
        # Search for each match of pattern's first word. Increment counter for both loops and check if each word matches. Once pattern completes return True else False
        words_in_line = line.rstrip().split(' ')
        words_in_pattern = pattern.rstrip().split(' ')
        match = False
        candidate_patterns = []
        new_pattern = ""
        for i in range(len(words_in_line)):
            if words_in_pattern[0] == words_in_line[i] and ((i+len(words_in_pattern)) < len(words_in_line)) :    # If the first word matches then iterate through pattern and check if rest of the words match
                match = True
                for j in range(len(words_in_pattern)):
                    if words_in_pattern[j] != words_in_line[i+j]:
                        match = False
                if match == True:
                    # Match found return match string + 1 word
                    new_pattern = pattern + " " + words_in_line[i+len(words_in_pattern)]
                    candidate_patterns.append(new_pattern)
        # Return only unique patterns
        unique_candidate_patterns = []
        [unique_candidate_patterns.append(x) for x in candidate_patterns if x not in unique_candidate_patterns]
        return unique_candidate_patterns


    def generate_all_frequent_patterns(self):
        # Iterate through each line and search for occurance of each length-(n-1) pattern.
        # If found add the next word and store in temporary frequent patterns
        # Prune the temporary frequent pattern list to generate patterns for 'length'
        # Iterate loop till temporary frequent patterns (self.n_1_patterns) list is not empty.
        iter = 1
        while(len(self.n_1_patterns) > 0):
            for pattern in self.n_1_patterns:
                for line in self.lines:
                    candidate_patterns = self.pattern_match(line, pattern)
                    if len(candidate_patterns) > 0:
                        for new_pattern in candidate_patterns:
                            if new_pattern not in self.n_patterns:
                                self.n_patterns[new_pattern] = 1
                            else:
                                self.n_patterns[new_pattern] = int(self.n_patterns[new_pattern]) + 1
            self.n_1_patterns = []
            # Save only patterns > threshold
            if iter == 2:
                print(self.n_patterns)
            for key, value in self.n_patterns.items():
                if value > self.absolute_support:
                    self.output(key,value)
                    print(key, value)
                    self.n_1_patterns.append(key)
            self.n_patterns = {}
            iter += 1



    def output(self, key, value):
        with open("patterns.txt", 'a') as file:
            line = str(value) + ":"
            words = key.rstrip().split(' ')
            for word in words:
                line += word + ";"
            line = line[:-1]
            line += "\n"
            file.write(line)


if __name__ == '__main__':

    sequential_patterns = SequentialPatterns()

    #Iterate and read each line and store locally
    filename = "reviews_samples.txt"
    for line in open(filename,'r'):
        sequential_patterns.insert_line(line)

    #Generate frequency 1 pattern list
    sequential_patterns.generate_frequent_1_patterns()

    #sequential_patterns.generate_candidate_patterns_and_count()

    sequential_patterns.generate_all_frequent_patterns()



